<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gambar extends Model
{
  protected $table = "gambar";

  protected $fillable = ['file','title','keterangan','link','type','name'];

  public function user()
  {
      return $this->belongsTo(User::class);
  }

}
