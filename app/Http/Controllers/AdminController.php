<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Gambar;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin-panel');
    }

    public function photo()
    {
        $user = Auth::user();
        if($user->name == "admin"){
            $userName = null;
        } else {
            $userName = $user->name;
        }
        $gambar = Gambar::where('name',$userName)->get();
    		return view('admin-panel-photo',['gambar' => $gambar]);

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function respas()
    {
        return view('auth.passwords.passresetnomail');
    }

    public function respascon(Request $request)
    {

      $this->validate($request,[
        'oldpassword' => 'required|string',
        'password' => 'required|confirmed'
      ]);

      $hashedPassword = Auth::user()->password;

      if(Hash::check($request->oldpassword,$hashedPassword)) {
        $user = User::find(Auth::id());

        $user->password = Hash::make($request->password);

        $user->save();

        Auth::logout();

        toastr()->success("You have successfully Edited Your Password!", "Password Edited");
        return redirect()->route('login');

      }
      else {
        // toastr()->error("You have not successfully Edited Your Password!", "OldPassword does not Matched");
        throw ValidationException::withMessages(['oldpassword' => 'Password wrong']);
        return redirect()->back();
      }

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gambar = DB::select('select * from gambar where id = ?',[$id]);
        return view('admin-panel-photo',['gambar' => $gambar]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit(Request $request,$id)
       {

         $edit = Gambar::find($request->id);
               $edit->title = $request->title;
               $edit->keterangan = $request->keterangan;
               $edit->type = $request->type;
               $edit->save();

         if (  $edit->save() ) {
           toastr()->success("You have successfully Edited the data!", "Data Edited");
           return redirect()->back();
         }
          else {
           toastr()->error("You have not successfully Edited the data!", "Data not Edited");
            return redirect()->back();
          }

         }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
         Gambar::find($id)->delete($id);

       if ($id) {
         toastr()->success("You have successfully Deleted The data!", "Data Deleted");
         return redirect()->back();

       }

       else {
         toastr()->error("You have not successfully Deleted the data!", "Data not Deleted");
          return redirect()->back();
       }

     }
}
