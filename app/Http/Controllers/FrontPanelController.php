<?php

namespace App\Http\Controllers;

use App\Gambar;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Auth;

class FrontPanelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // $gambar = Gambar::get();

      $gambar = Gambar::Where("name", null)->orderby('created_at','desc')->paginate(9);
      $name = null;
      $islogin = false;
      return view('front-panel',compact('gambar'),['name' => $name , 'islogin' => $islogin]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function user($name)
    {
        if ($name == 'admin') {
            $name = null;
        }
        //dd($name);
        // $gam = DB::select('select * from gambar where name = ?',[$name]);
        // dd($gam);

        $gambar = Gambar::Where("name",$name)->orderby('created_at','desc')->paginate(9);
        // dd($gambar->count());

        $user = Auth::user();
        if($user){
            $islogin = True;
        } else {
            $islogin = False;
        }

        if ($gambar->count() > 0) {
            return view('front-panel',['gambar' => $gambar],['name' => $name, 'islogin' => $islogin]);
        }

        // elseif ($name = "admin") {
        //     return view('auth.login');
        // }

        else {
            $userCheck = User::where('name',$name)->first();
            if (!$userCheck) {
                // return view('user-not-found');
                if ($user) {
                    return view('user-not-found');
                } else {
                    return redirect()->route('register');
                }

            } else {
                return redirect()->route('contentadmin');
            }

        }

    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
