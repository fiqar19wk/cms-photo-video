<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Gambar;

use Intervention\Image\Facades\Image;

class UploadController extends Controller
{
	public function upload(){
		$gambar = Gambar::get();
		return view('front-panel',['gambar' => $gambar]);

	}

	public function proses_upload(Request $request){


		$messages = [
    'required' => ':attribute is required to fill',
    'max' => ':attribute is to big to continue it needs to be at least this big :max ',
		];


		$this->validate($request, [
			'file' => 'required|file|image|max:2400',
			'title' => 'required',
			'link' => 'required',
			'type' => 'required',

		],$messages);



		// menyimpan data file yang diupload ke variabel $file
		$file = $request->file('file');

		$nama_file = time()."_".$file->getClientOriginalName();

      	        // isi dengan nama folder tempat kemana file diupload
		$tujuan_upload = 'data_file';
		$image = Image::make(	$file->move($tujuan_upload,$nama_file) )->fit(400, 400);
		$image->save();


		Gambar::create([
			'file' => $nama_file,
			'title' => $request->title,
			'keterangan' => $request->keterangan,
			'link' => $request->link,
			'type' => $request->type,
			'name' => $request->name,
		]);

		if ($file) {
			toastr()->success("You have  successfully Added The Image!", "Image Added");
			return redirect()->back();

		}


	}

	public function upload_crop(Request $request){


				$messages = [
		    'required' => ':attribute is required to fill',
		    'max' => ':attribute is to big to continue it needs to be at least this big :max ',
				];

				$this->validate($request, [
					'image' => 'required',
				 	'title' => 'required',
					'link' => 'required',
				 	'type' => 'required',

				 ],$messages);

				$image = $request->image;



			 list($type, $image) = explode(';', $image);

			 list(, $image)      = explode(',', $image);

			 $image = base64_decode($image);

			 $image_name= time().'.png';

			 $path = public_path('data_file/'.$image_name);



			 file_put_contents($path, $image);


				$gambar = Gambar::create([
					'file' =>  $image_name,
					'title' => $request->title,
					'keterangan' => $request->keterangan,
					'link' => $request->link,
					'type' => $request->type,
					'name' => $request->name,
				]);

				if (isset($gambar->id)) {
					toastr()->success("You have  successfully Added The Image!", "Image Added");
			    return response()->json(['status'=>true]);
					// return redirect()->back();

				}

	}



}
