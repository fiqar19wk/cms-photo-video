<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'email' => 'admin@gg.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$QrrGTFdXarDcefSpJLbAA.rWpFhg49mZN13QXdDCe9QjzCOPNSggm',
                'remember_token' => NULL,
                'created_at' => '2020-02-20 15:37:07',
                'updated_at' => '2020-02-20 15:37:07',
            ),
        ));
        
        
    }
}