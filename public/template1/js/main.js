(function($) {

    "use strict";

    $('.main-navigation a').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html,body').animate({
              scrollTop: target.offset().top
            }, 1000);
            return false;
          }
        }
    });

    $('.toggle-menu').click(function(){
        $('.main-navigation ul').stop(true,true).slideToggle();
    });

    $(window).on('resize', function(){
        var win = $(this); //this = window
        if (win.width() >= 769) {
            $('.main-navigation ul').css( "display", "block" );
        }
        if (win.width() <= 768) {
            $('.main-navigation ul').css( "display", "none" );
        }
    });




})(jQuery);
