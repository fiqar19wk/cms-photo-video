@extends('admin.index')


@section('styles')
<!-- dataTables css -->
<link href="{{ asset('coreui/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">


<!-- toastr css -->
<link href="{{ asset('coreui/vendors/toastr/css/toastr.min.css') }}" rel="stylesheet">

<!-- croppie css and script -->
<link rel="stylesheet" href="{{ asset('css/croppie.css')}}" />


@endsection



@section('content')


<div class="container-fluid">
  <div class="animated fadeIn">
    <div class="card">
      <div class="card-body">
        <table class="table table-striped table-bordered datatable" id="table_id">

          <thead>
            <tr>
              <th>No</th>
              <th>Title</th>
              <th>File</th>
              <th>Description</th>
              <th>Type</th>

              <th>Created at</th>
              <th>Uploaded By</th>
              <th>Actions</th>
            </tr>
          </thead>

            @foreach ($gambar as $ga)
				<tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $ga->title }}</td>
                    <td><img width="150px" src="{{  asset('/data_file/'.$ga->file) }}"></td>
                    <td>{{$ga->keterangan}}</td>
                    <td>{{ $ga->type }}</td>

                    <td>{{ $ga->created_at }}</td>
                    <td>{{ $ga->name }}</td>
					<td> <button class="btn btn-danger mb-1 deleteB" type="button" data-toggle="modal" data-target="#dangerModal" data-id="{{ $ga->id }}">Delete</button>
                        <button class="btn btn-primary mb-1 editB" type="button" data-toggle="modal" data-target="#editModal" data-id="{{ $ga->id }}">Edit Data</i></button>
                    </td>
				</tr>
		    @endforeach



        </table>

      </div>
      <div class="card-footer">

                <div class="form-group row" width="100%" >

                    <div class="col-md-4" align="left" >
                        <a href="#"><button class="btn btn-primary px-4 " type="button" ><i class="fa fa-arrow-left fa-md mt-1"></i> BACK</button></a>
                        <a class="userview" id="userview" href=""  target="_blank"><button class="btn btn-primary px-4 user" type="button" user-id="{{ Auth::user()->name }}" ><i class="fa fa-eye fa-md mt-1"></i></i> View My Page</button></a>
                    </div>


                    <div class="col-md-8" align="right">
                        <button class="btn btn-primary mb-1" type="button" data-toggle="modal" data-target="#uploadimageModal">ADD <i class="fa fa-plus fa-md mt-1"></i></button>
                      <!-- <button class="btn btn-primary mb-1" type="button" data-toggle="modal" data-target="#primaryModal">ADD <i class="fa fa-plus fa-md mt-1"></i></button> -->
                    </div>



                </div>
      </div>
    </div>
  </div>
</div>

<!-- /.modal-->

        <div class="modal fade" id="dangerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-danger" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Deleting Data</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <p>Your about to delete a User data are you sure?</p>
              </div>
              <div class="modal-footer">
                <form id="deleteModalForm" action="" method="POST">
                    @method('DELETE')
                    @csrf
                <button class="btn btn-secondary" type="button" data-dismiss="modal">No</button>
                <button class="btn btn-danger" type="submit">Yes</button>
                </form>
              </div>
            </div>
            <!-- /.modal-content-->
          </div>
          <!-- /.modal-dialog-->
        </div>


        <!-- /.modal photo-->
        <div class="modal fade" id="primaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-primary modal-xl" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Add File</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="{{ route('photo')}}" method="POST" enctype="multipart/form-data">
                  {{ csrf_field() }}

                  <div class="form-group">
                    <b>Title</b>
                    <input class="form-control" type="text" name="title" >
                  </div>

                  <div class="form-group">
                    <b>Content type</b>
                    <select class="form-control" name="type">
                      <option></option>
                      <option value="video">Video</option>
                      <option value="photo">Photo</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <b>Insert Thumbnail Here, Images Biger than 400x400 will be automatically to be fit to 400x400</b><br/>
                    <input id="file-input" type="file" name="file">
                  </div>



                  <div class="form-group">
                    <b>Link</b>
                    <input class="form-control" type="text" name="link">
                  </div>

                  <div class="form-group">
                    <b>Description</b>
                    <textarea class="form-control" name="keterangan"></textarea>
                  </div>

                  <div class="form-group">
                    <b>Uploader</b>
                    <input class="form-control" type="text" name="name" value="{{ Auth::user()->name }}" readonly>
                  </div>



              </div>
              <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" type="submit" value="Upload" >Upload File</button>
                </form>
              </div>
            </div>

            <!-- /.modal-content-->
          </div>
          <!-- /.modal-dialog-->
        </div>

        <!-- /.modal photo-->
        <div class="modal fade" id="uploadimageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-primary modal-xl" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Add File</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="{{ route('crop')}}" method="POST" enctype="multipart/form-data">
                  {{ csrf_field() }}

                  <div class="form-group">
                    <b>Title</b>
                    <input class="form-control" type="text" name="title" id="title">
                  </div>

                  <div class="form-group">
                    <b>Content type</b>
                    <select class="form-control" name="type" id="type">
                      <option></option>
                      <option value="video">Video</option>
                      <option value="photo">Photo</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <b>Insert image here to be croped manualy</b><br/>
                    <input id="crop" type="file" name="file">
                  </div>

                  <div class="form-group">
                    <div id="image_demo" style="width:100%"></div>
                  </div>

                  <div class="form-group">
                    <b>Link</b>
                    <input class="form-control" type="text" name="link" id="link">
                  </div>

                  <div class="form-group">
                    <b>Description</b>
                    <textarea class="form-control" name="keterangan" id="ket"></textarea>
                  </div>

                  <!-- <div class="form-group">
                    <b>Uploader</b> -->
                    @if ( Auth::user()->name == "admin")
                    <input class="form-control" type="hidden" id="name" name="name" value="" readonly>
                    @else
                    <input class="form-control" type="hidden" id="name" name="name" value="{{ Auth::user()->name }}" readonly>
                    @endif
                  <!-- </div> -->


              </div>
              <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                <button class="btn btn-primary crop_image" type="button" value="Upload" >Upload File</button>
                </form>
              </div>
            </div>

            <!-- /.modal-content-->
          </div>
          <!-- /.modal-dialog-->
        </div>



        <!-- /.modal-->
        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-primary" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Edit Photo</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <form id="editModalForm" action="" method="POST" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <div class="form-group">
                    <b>Title</b>
                    <input class="form-control titleE" type="text" name="title" id="titleid">
                  </div>
                  <div class="form-group">
                    <b>Content type</b>
                    <select class="form-control" name="type" id="edittype">
                      <option value=""></option>
                      <option value="video">Video</option>
                      <option value="photo">Photo</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <b>Description</b>
                    <textarea class="form-control" name="keterangan" id="editketerangan"></textarea>
                  </div>
                  <!-- <div class="form-group">
                    <b>link</b>
                    <textarea class="form-control" name="link" id="editlink"></textarea>
                  </div> -->
              </div>
              <div class="modal-footer">

                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit" value="Upload" >Edit File</button>


                </form>
              </div>
            </div>

            <!-- /.modal-content-->
          </div>
          <!-- /.modal-dialog-->
        </div>


    </div>
  </div>
</div>

@endsection



@section('javascript')
<!-- dataTables script -->
<script src="{{ asset('coreui/node_modules/datatables.net/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('coreui/node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('coreui/js/datatables.js') }}"></script>
<script>
  $(document).ready( function () {
  $('#table_id').DataTable();
  } );
</script>

<!-- modal Script -->

<script>
  $(document).ready( function () {

    $("#table_id").on('click','.deleteB',function () {
                  var formAction = "{{ route('destroy', ['id' => null]) }}" + "/" + $(this).attr("data-id");
                  $("#deleteModalForm").attr("action", formAction);
              });
  // $("button.deleteB").click(function () {
  //               var formAction = "{{ route('destroy', ['id' => null]) }}" + "/" + $(this).attr("data-id");
  //               $("#deleteModalForm").attr("action", formAction);
  //           });
  } );
</script>

<script>


$("#userview").on('click','.user',function () {
              var viewHref = "{{ route('userindex', ['name' => null]) }}" + "/" + $(this).attr("user-id");
              $(".userview").attr("href", viewHref);
          });

</script>

<script>
  $(document).ready( function () {
    $("button.editB").click(function () {
        $tr = $(this).closest('tr')

        $("#edittype").val([]);

        var data = $tr.children("td").map(function(){
            return $(this).text();
        }).get();

        $('#titleid').val(data[1]);
        $("#edittype option[value='"+data[4]+"']").attr("selected", "selected");
        $('#editketerangan').val(data[3]);
        // $('#editlink').val(data[5]);

        var formAction = "{{ route('edit', ['id' => null]) }}" + "/" + $(this).attr("data-id");
        $("#editModalForm").attr("action", formAction);
    });
  } );
</script>

<!-- main toastr script -->
<script src="{{ asset('coreui/node_modules/toastr/toastr.js')}}"></script>
<script src="{{ asset('coreui/js/toastr.js')}}"></script>
{!! toastr()->render() !!}



<!-- Cropie script -->

<script>
$(document).ready(function(){

 $image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:400,
      height:400,
      type:'square'
    },
    boundary:{
      width:450,
      height:450,
    }
  });

  $('#crop').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
  });

  $('.crop_image').click(function(event){
    $(this).html('<span class="spinner-border spinner-border-sm"></span> Loading..');
    $(this).attr('disabled','disabled');
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
      $.ajax({
        url:"{{ route('crop')}}",
        type: "POST",

        data:{
          "_token" : "{{ csrf_token() }}",
          "image": response,
          "title": $('#title').val(),
          "type":  $("#type option:selected").val(),
          "link": $('#link').val(),
          "keterangan": $('#ket').val(),
          "name": $('#name').val(),

        },
        success: function (data) {

          location.reload();
			}
      }).then(function(){
        console.log('Success');
      });
    })
  });

});
</script>
<script src="{{ asset('js/croppie.js')}}"></script>
<script src="{{ asset('js/bootstrap.min.js')}}"></script>

@endsection
