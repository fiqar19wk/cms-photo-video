<aside class="aside-menu">
  <ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" href="#settings" role="tab">
        <i class="icon-settings"></i>
      </a>
    </li>
  </ul>
  <!-- Tab panes-->
  <div class="tab-content">
    <div class="tab-pane active" id="timeline" role="tabpanel">
      <div class="list-group list-group-accent">
        <div class="list-group-item list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small">Settings</div>
        <div class="list-group-item list-group-item-accent-danger list-group-item-divider">
          <a href="{{ route('respas') }}"><div>Change Password
          </div></a>

        </div>
        <!-- <div class="list-group-item list-group-item-accent-danger list-group-item-divider">
          <div>Change Username
          </div>
          <small class="text-muted">
            Not Yet implemented</small>
        </div> -->
        <!-- <div class="list-group-item list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small">Tomorrow</div>
        <div class="list-group-item list-group-item-accent-danger list-group-item-divider">
          <div>New UI Project -
            <strong>deadline</strong>
          </div>
          <small class="text-muted mr-3">
            <i class="icon-calendar"></i>  10 - 11pm</small>
          <small class="text-muted">
            <i class="icon-home"></i>  creativeLabs HQ</small>
          <div class="avatars-stack mt-2">
            <div class="avatar avatar-xs">
              <img class="img-avatar" src="{{ asset('coreui/img/avatars/2.jpg')}}" alt="admin@bootstrapmaster.com">
            </div>
            <div class="avatar avatar-xs">
              <img class="img-avatar" src="{{ asset('coreui/img/avatars/3.jpg')}}" alt="admin@bootstrapmaster.com">
            </div>
            <div class="avatar avatar-xs">
              <img class="img-avatar" src="{{ asset('coreui/img/avatars/4.jpg')}}" alt="admin@bootstrapmaster.com">
            </div>
            <div class="avatar avatar-xs">
              <img class="img-avatar" src="{{ asset('coreui/img/avatars/5.jpg')}}" alt="admin@bootstrapmaster.com">
            </div>
            <div class="avatar avatar-xs">
              <img class="img-avatar" src="{{ asset('coreui/img/avatars/6.jpg')}}" alt="admin@bootstrapmaster.com">
            </div>
          </div>
        </div>
        <div class="list-group-item list-group-item-accent-success list-group-item-divider">
          <div>
            <strong>#10 Startups.Garden</strong> Meetup</div>
          <small class="text-muted mr-3">
            <i class="icon-calendar"></i>  1 - 3pm</small>
          <small class="text-muted">
            <i class="icon-location-pin"></i>  Palo Alto, CA</small>
        </div>
        <div class="list-group-item list-group-item-accent-primary list-group-item-divider">
          <div>
            <strong>Team meeting</strong>
          </div>
          <small class="text-muted mr-3">
            <i class="icon-calendar"></i>  4 - 6pm</small>
          <small class="text-muted">
            <i class="icon-home"></i>  creativeLabs HQ</small>
          <div class="avatars-stack mt-2">
            <div class="avatar avatar-xs">
              <img class="img-avatar" src="{{ asset('coreui/img/avatars/2.jpg')}}" alt="admin@bootstrapmaster.com">
            </div>
            <div class="avatar avatar-xs">
              <img class="img-avatar" src="{{ asset('coreui/img/avatars/3.jpg')}}" alt="admin@bootstrapmaster.com">
            </div>
            <div class="avatar avatar-xs">
              <img class="img-avatar" src="{{ asset('coreui/img/avatars/4.jpg')}}" alt="admin@bootstrapmaster.com">
            </div>
            <div class="avatar avatar-xs">
              <img class="img-avatar" src="{{ asset('coreui/img/avatars/5.jpg')}}" alt="admin@bootstrapmaster.com">
            </div>
            <div class="avatar avatar-xs">
              <img class="img-avatar" src="{{ asset('coreui/img/avatars/6.jpg')}}" alt="admin@bootstrapmaster.com">
            </div>
            <div class="avatar avatar-xs">
              <img class="img-avatar" src="{{ asset('coreui/img/avatars/7.jpg')}}" alt="admin@bootstrapmaster.com">
            </div>
            <div class="avatar avatar-xs">
              <img class="img-avatar" src="{{ asset('coreui/img/avatars/8.jpg')}}" alt="admin@bootstrapmaster.com">
            </div>
          </div>
        </div> -->
      </div>
    </div>
  </div>
</aside>
