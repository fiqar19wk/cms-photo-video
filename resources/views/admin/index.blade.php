<!DOCTYPE html>
<!--
* CoreUI Pro - Bootstrap Admin Template
* @version v2.1.14
* @link https://coreui.io/pro/
* Copyright (c) 2018 creativeLabs Łukasz Holeczek
* License (https://coreui.io/pro/license)
-->

<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>Klik Profil | Admin {{ Auth::user()->name }}</title>
    <!-- Icons-->
    {{-- <link rel="icon" type="image/ico" href="{{ asset('./coreui/img/favicon.ico')}}" sizes="any" /> --}}
    <link href="{{ asset('coreui/node_modules/@coreui/icons/css/coreui-icons.min.css')}}" rel="stylesheet">
    <link href="{{ asset('coreui/node_modules/flag-icon-css/css/flag-icon.min.css')}}" rel="stylesheet">
    <link href="{{ asset('coreui/node_modules/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{ asset('coreui/node_modules/simple-line-icons/css/simple-line-icons.css')}}" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="{{ asset('coreui/css/style.css')}}" rel="stylesheet">
    <link href="{{ asset('coreui/vendors/pace-progress/css/pace.min.css')}}" rel="stylesheet">
    @yield('styles')


  </head>
  <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    <!-- header -->
    @include('admin.header')
    @section('header')
    @show
    <div class="app-body">
      <!-- sidebar -->
        @include('admin.sidebar')
        @section('sidebar')
        @show
      <main class="main">
        <!-- Breadcrumb-->
        <ol class="breadcrumb">
          {{-- <li class="breadcrumb-item">Home</li>
          <li class="breadcrumb-item">
            <a href="#">Admin</a>
          </li>
          <li class="breadcrumb-item active">Dashboard</li> --}}
          <!-- Breadcrumb Menu-->

        </ol>
        @yield('content')
      </main>
      <!-- aside -->
        @include('admin.aside')
        @section('aside')
        @show
    </div>
    <!-- footer -->
      @include('admin.footer')
      @section('footer')
      @show

    <!-- CoreUI and necessary plugins-->
    <script src="{{ asset('coreui/node_modules/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{ asset('coreui/node_modules/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{ asset('coreui/node_modules/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('coreui/node_modules/pace-progress/pace.min.js')}}"></script>
    <script src="{{ asset('coreui/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js')}}"></script>
    <script src="{{ asset('coreui/node_modules/@coreui/coreui-pro/dist/js/coreui.min.js')}}"></script>
    <!-- Plugins and scripts required by this view-->
    <!-- <script src="{{ asset('coreui/node_modules/chart.js/dist/Chart.min.js')}}"></script> -->
    <script src="{{ asset('coreui/node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/js/custom-tooltips.min.js')}}"></script>
    <script src="{{ asset('coreui/js/main.js')}}"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    @yield('javascript')

  </body>
</html>
