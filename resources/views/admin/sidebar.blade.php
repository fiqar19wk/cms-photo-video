<div class="sidebar">
  <nav class="sidebar-nav">
    <ul class="nav">
      <li class="nav-item">
        {{-- <a class="nav-link" href="{{ route('admin') }}">
          <i class="nav-icon icon-speedometer"></i> Dashboard
          <span class="badge badge-info">NEW</span>
        </a> --}}
      </li>

      <li class="nav-item nav-dropdown">
            <a class="nav-link" href="{{ route('contentadmin') }}">
                <i class="nav-icon fa fa-folder-o fa-md"></i></i> Content
            </a>
                {{-- <ul class="nav-dropdown-items">
                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('photoadmin') }}">
                        <i class="nav-icon icon-cursor"></i> Photo</a>
                    </li>
                </ul> --}}

                <a class="nav-link" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                              <i class="nav-icon fa fa-sign-out fa-md"></i></i>
                {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>


        </li>

    </ul>
  </nav>
  <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
