<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                          <div class="card-header">{{ __('Login') }}</div>
                          <div class="card-body">
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group">
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text">Email</span>
                                    </div>
                                    <input class="form-control @error('email') is-invalid @enderror" id="email" type="email" name="email"  value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                  </div>
                                </div>

                              <div class="form-group">
                                <div class="input-group">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text">Password</span>
                                  </div>
                                  <input class="form-control @error('password') is-invalid @enderror" id="password" type="password" name="password" required autocomplete="current-password">

                                  @error('password')
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                  @enderror

                                </div>
                              </div>
                              <div class="form-group form-actions">
                                <button class="btn btn-sm btn-success col-md-2" type="submit">Login</button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                              </div>
                            </form>
                          </div>
                          @if (Route::has('register'))
                        <div class="card-footer" width="100%">

                          <div class="form-group row" width="100%" >

                              <div class="col-md-6" align="left" >
                                  Click the register button if you don't have an Account
                              </div>

                              <div class="col-md-6" align="right">
                                <div class="col-md-8 offset-md-4">
                                  <a href="{{ route('register') }}">
                                    <button class="btn btn-primary">
                                        {{ __('Register') }}
                                    </button>
                                    <a>
                                </div>
                              </div>
                          </div>


                        </div>
                        @endif

                        </div>



                    </div>
                </div>
            </div>
        </main>
    </div>
    <!-- main toastr script -->
    <script src="{{ asset('coreui/node_modules/toastr/toastr.js')}}"></script>
    <script src="{{ asset('coreui/js/toastr.js')}}"></script>
    {!! toastr()->render() !!}
</body>
</html>
