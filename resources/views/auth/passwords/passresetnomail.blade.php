<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
  <div id="app">
      <main class="py-4">
          <div class="container">
              <div class="row justify-content-center">
                  <div class="col-md-8">
                      <div class="card">
                          <div class="card-header">{{ __('Reset Password') }}</div>

                          <div class="card-body">
                              <form method="POST" action="{{ route('respascon') }}">
                                  @csrf

                                  <div class="form-group">
                                    <div class="input-group">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text">Old Password</span>
                                      </div>
                                      <input class="form-control @error('oldpassword') is-invalid @enderror" id="oldpassword" type="password" name="oldpassword" required autofocus>

                                      @error('oldpassword')
                                          <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                          </span>
                                      @enderror

                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <div class="input-group">
                                      <div class="input-group-prepend">
                                        <span for="password" class="input-group-text">{{ __('New Password') }}</span>
                                      </div>
                                      <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                      @error('password')
                                          <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                          </span>
                                      @enderror
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <div class="input-group">
                                      <div class="input-group-prepend">
                                        <span for="password-confirm" class="input-group-text">{{ __('ConfirmPassword') }}</span>
                                      </div>
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                  </div>

                                  <div class="form-group row mb-0">
                                      <div class="col-md-6 offset-md-4">
                                        <a href="{{ route('contentadmin')}}"><button type="button" class="btn btn-danger" align="left">
                                            {{ __('Back') }}
                                        </button></a>
                                          <button type="submit" class="btn btn-primary" align="right">
                                              {{ __('Reset Password') }}
                                          </button>
                                      </div>
                                  </div>
                              </form>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </main>
  </div>
</body>
</html>
