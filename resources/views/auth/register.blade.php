<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
  <div id="app">
      <main class="py-4">
        <div class="container">
          <div class="row justify-content-center">
              <div class="col-md-8">

                <div class="card">
                  <div class="card-header">Register</div>
                  <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                      <div class="form-group">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span for="name" class="input-group-text">{{ __('Name') }}</span>
                          </div>
                          <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                          @error('name')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror

                        </div>
                      </div>
                      <div class="form-group">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span for="email" class="input-group-text">{{ __('E-Mail') }}</span>
                          </div>
                          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                          @error('email')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span for="password" class="input-group-text">{{ __('Password') }}</span>
                          </div>
                          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                          @error('password')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span for="password-confirm" class="input-group-text">{{ __('ConfirmPassword') }}</span>
                          </div>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>
                      </div>
                      <div align="right" class="form-group form-actions">
                        <button class="btn btn-sm btn-primary col-md-2" type="submit">Submit</button>
                      </div>
                    </div>

                    </form>
                  </div>
                  <div class="card-footer" width="100%">

                    <div class="form-group row" width="100%" >

                        <div class="col-md-6" align="left" >
                            Click the login button if you allready have an Account
                        </div>

                        <div class="col-md-6" align="right">
                          <div class="col-md-8 offset-md-4">
                            <a href="{{ route('login') }}">
                              <button class="btn btn-primary">
                                  {{ __('Login') }}
                              </button>
                              <a>
                          </div>
                        </div>
                    </div>


                  </div>
                </div>






</body>
</html>
