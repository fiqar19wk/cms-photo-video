@extends('front.index')


@section('content')

<div class="main-posts">
    <div class="container">
        <div class="infinite-scroll">
        <div class="w3-row-padding">

            <div class="blog-masonry">

              @foreach ($gambar as $g)

                <div class="post-masonry w3-col s4 ">
                    <div class="post-thumb">
                        <a href=" {{$g->link}} " class="link_direct" target="_blank">
                            <img src="{{ asset('/data_file/'.$g->file) }}" alt="">
                            <div class="w3-display-topright w3-container">
                                <h6>

                                   @if ($g->type == 'video')
                                      <i class="fa fa-video-camera" ></i>
                                    @else
                                    <i class="fa fa-picture-o"></i>

                                  @endif

                                </h6>
                            </div>
                        </a>
                        {{-- <div class="post-hover text-center">
                            <a href=" {{$g->link}} " class="link_direct">
                                <div class="inside">
                                    <i class="fa fa-picture-o"></i>
                                    <span class="date">{{$g->created_at}}</span>
                                    <p>{{$g->keterangan}}</p>
                                </div>
                            </a>
                        </div> --}}
                    </div>
                </div> <!-- /.post-masonry -->

                @endforeach



                  {{$gambar->links()}}

                      </div>

                  </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script>
        // $(document).ready(function(){
        //     $('#link_direct').click(function(event){
        //         event.preventDefault();
        //         // window.location.replace($(this).attr("href"));
        //     })
        // });
    </script>
    <script type="text/javascript">
        $('ul.pagination').hide();
        $(function() {
            $('.infinite-scroll').jscroll({
                autoTrigger: true,
                loadingHtml: '<center><img class="center-block" src="{{ asset('spinner2.gif') }}" alt="Loading..." /></center>',
                padding: 0,
                nextSelector: '.pagination li.active + li a',
                contentSelector: 'div.infinite-scroll',
                callback: function() {
                    $('ul.pagination').remove();
                }
            });
        });
    </script>
@endpush
