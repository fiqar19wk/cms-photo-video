<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>@if ($name == null)
        Klik Profil
        @else
        {{$name}}
        @endif</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="{{ asset('template1/css/normalize.css') }}">
        <link rel="stylesheet" href="{{ asset('template1/css/font-awesome.css') }}">
        <link rel="stylesheet" href="{{ asset('template1/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('template1/css/templatemo-style.css')}}">
        <script src="{{ asset('template1/js/vendor/modernizr-2.6.2.min.js') }}"></script>
        <link rel="stylesheet" href="{{ asset('css/w3.css') }}">
        <!--
        Masonry Template
        http://www.templatemo.com/preview/templatemo_434_masonry
        -->
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div id="loader-wrapper">
            <div id="loader"></div>
        </div>

        <div class="content-bg"></div>
        <div class="bg-overlay"></div>

        <!-- SITE TOP -->
        @include('front.site-top')
        @section('site-top')
        @show

        <!-- MAIN POSTS -->
        @yield('content')

        <!-- FOOTER -->
        @include('front.footer')
        @section('footer')
        @show


        <script src="{{ asset('template1/js/vendor/jquery-1.10.2.min.js')}}"></script>
        <script src="{{ asset('template1/js/min/plugins.min.js')}}"></script>
        <script src="{{ asset('template1/js/min/main.min.js')}}"></script>
        <script src="{{ asset('template1/js/main.js')}}"></script>
        <script src="{{ asset('js/jquery.jscroll.js')}}"></script>

        <!-- Preloader -->
        <script type="text/javascript">
            //<![CDATA[
            $(window).load(function() { // makes sure the whole site is loaded
                $('#loader').fadeOut(); // will first fade out the loading animation
                    $('#loader-wrapper').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
                $('body').delay(350).css({'overflow-y':'visible'});
            })
            //]]>
        </script>
        @stack('scripts')
	<!-- templatemo 434 masonry -->
    </body>
</html>
