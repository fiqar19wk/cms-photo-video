<div class="site-top">
    <div class="site-header clearfix">
        <div class="container">
            {{-- <a href="#" class="site-brand pull-left"><strong>Masonry</strong> Free Template</a> --}}
            <a href="#" class="site-brand pull-left"strong>
              @if ($name == null)
              Klik Profil
              @else
              {{$name}}
              @endif
            </a>
            <div class="main-navigation">
                <a href="#" class="visible-xs toggle-menu"><i class="fa fa-bars"></i></a>
                <nav class="site-navigation">
                    <ul>
                        @if($islogin)
                            <li><a href="{{route("contentadmin")}}">Upload</a></li>
                        @else
                            <li><a href="{{route("register")}}">Register</a></li>
                            <li><a href="{{route("login")}}">Login</a></li>
                        @endif
                    </ul>
                </nav>
            </div> <!-- .main-navigation -->
        </div>
    </div> <!-- .site-header -->
    <div class="site-banner">
        <div class="container">
            <div class="row">

            </div>
            <div class="row">


            </div>
        </div>
    </div> <!-- .site-banner -->
</div> <!-- .site-top -->
