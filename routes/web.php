<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes(['verify' => true]);

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'FrontPanelController@index')->name('front');


Route::get('/{name}', 'FrontPanelController@user')->name('userindex');

Route::get('admin/menu', 'AdminController@index')->name('admin')->middleware('verified');
Route::get('admin/contentadmin', 'AdminController@photo')->name('contentadmin')->middleware('verified');

Route::delete('delete/{id}','AdminController@destroy')->name('destroy');

Route::get('edit/{id}','AdminController@show')->name('show');
Route::post('edit/{id}','AdminController@edit')->name('edit');
Route::get('admin/resetpass', 'AdminController@respas')->name('respas')->middleware('verified');
Route::post('admin/resetpasscon', 'AdminController@respascon')->name('respascon')->middleware('verified');
Route::get('/upload', 'UploadController@upload')->name('upload');

Route::post('/upload/proses', 'UploadController@proses_upload')->name('photo');

Route::post('/upload/crop', 'UploadController@upload_crop')->name('crop');
